from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city}, {state}",
        "per_page": "1",
    }

    # Create the URL for the request with the city and state
    url = "https://api.pexels.com/v1/search"
    # Make the request
    r = requests.get(url, params=params, headers=headers)
    # Parse the JSON response
    content = r.json()
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{city}, {state}, US",
        "limit": "1",
        "appid": OPEN_WEATHER_API_KEY,
    }
    # Make the request
    r = requests.get(url, params=params)
    # Parse the JSON response
    content = r.json()
    # Get the latitude and longitude from the response
    try:
        lat = content[0]["lat"]
        lon = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    # Create the URL for the current weather API with the latitude
    #   and longitude
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = response.json()
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    weather = {
        "temperature": content["main"]["temp"],
        "description": content["weather"][0]["description"],
    }
    # Return the dictionary
    return weather
